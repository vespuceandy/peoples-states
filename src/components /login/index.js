import React from 'react';
import { Button, Form, Alert } from 'react-bootstrap';
import CryptoJS from 'crypto-js';
import { useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { useCookies } from 'react-cookie';
import db from '../../dbusers.json';
import './index.scss';

const Login = () => {
  const navigate = useNavigate();
  // eslint-disable-next-line no-unused-vars
  const [cookie, setCookie] = useCookies(['login', 'username']);
  const {
    register, handleSubmit, setError, formState, clearErrors
  } = useForm({
    mode: 'onChange'
  });
  const { isValid } = formState;
  const { errors } = formState;
  const wait = (duration = 1000) => new Promise((resolve) => {
    window.setTimeout(resolve, duration);
  });

  async function onSubmit(data) {
    const user = db.results.filter((v) => v.login.username === data.username);
    if (user.length !== 0) {
      const hashPasswordInput = CryptoJS.SHA256(data.password + user[0].login.salt).toString();
      if (user[0].login.sha256 === hashPasswordInput) {
        setError('errorMessage', { message: 'Bien joué tu as saisi les bons identifiants' });
        setCookie('login', true, { path: '/' });
        setCookie('username', data.username, { path: '/' });
        setTimeout(() => {
          navigate('/');
        }, 2000);
      } else {
        setError('errorMessage', { message: 'Mauvais mot de passe' });
      }
    } else {
      setError('errorMessage', { message: 'L\'utilisateur n\'existe pas' });
    }
    if (errors) {
      await wait(2000);
      clearErrors('errorMessage');
    }
  }

  return (
      <div className="Login">
         {errors.errorMessage && <Alert variant="danger">
          <p className="text-center">
                  {errors.errorMessage?.message}
          </p>
        </Alert>}
        {errors.username?.type === 'required'
          && <Alert variant="danger">
          <p className="text-center">
                  {errors.username?.message}
          </p>
        </Alert>}
        {errors.password?.type === 'required'
        && <Alert variant="danger">
        <p className="text-center">
                {errors.password?.message}
        </p>
      </Alert>}
        <h1 className='text-center' >People-Stats</h1>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group size="lg" controlId="username">
            <Form.Label>Pseudo : </Form.Label>
            <Form.Control
              type="username"
              // value={username}
              // onChange={(e) => setUsername(e.target.value)}
              {...register('username', { required: 'Veuillez saisir votre pseudo' })}
            />
          </Form.Group>
          <Form.Group size="lg" controlId="password">
            <Form.Label>Password : </Form.Label>
            <Form.Control
              type="password"
              // value={password}
              // onChange={(e) => setPassword(e.target.value)}
              {...register('password', { required: 'Veuillez saisir votre mot de passe' })}
            />
          </Form.Group>
          <Button size="lg" className="w-100 mt-2" type="submit" disabled={!isValid}>
            Login
          </Button>
        </Form>
      </div>
  );
};

export default Login;
